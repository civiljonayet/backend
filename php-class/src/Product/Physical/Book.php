<?php

namespace App\product\physical;

class Book{
    public $isbn = '';
    public $title = '';
    public $author = '';
    public $category = '';
    public $price = 0.0;
    public $isNew = null;
}

?>