<?php

namespace App\User;

//class User{
//    public $firstName;
//    public $lastName;
//
//    public function sayHello(){
//        echo 'Hello!';
//    }
//}

//class User{
//    public $firstName;
//    public $lastName;
//
//    public function sayHello(){
//        echo 'Hello! '.$this->firstName;
//    }
//
//    public function register(){
//        //echo '>> registered';
//        return $this->sayHello().", ". 'You are registered';
//    }
//
//    public function mail(){
//        //echo '>> email sent';
//        return $this->sayHello().", ". 'Email is sent';
//    }
//}

class User{
    public $firstName;
    public $lastName;

    public function __construct($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getFullName(){
        return $this->firstName." ".$this->lastName;
    }
}