<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Killer-Php</title>

    <?php include_once ('class_lib.php'); ?>

</head>
<body>

<?php
    //instantiate two different object form same class.
    $stefan = new Person(); //instance(creating object) of Person class.
    $jimmy =  new Person(); //instance(creating object) of Person Class.

    $stefan->setName('Stefan Mischook');//set name using methods
    $jimmy->setName('Nick Waddles');//set name using methods


    //get name using methods
    echo "Stefan's fullname is: ".$stefan->getName().'</br>';
    echo "Jimmy's fullname is: ".$jimmy->getName().'</br>';

    //create an object with constructor
    $jonayet = new Employee("Engr. Md. Jonayet Hasan");
    echo "Jonayet's fullname is: ". $jonayet->getName();

    $jabed = new Employee('Jonayet');
    //$jabed->setName('Jabed');
    echo "</br>" . $jabed->getName();
    echo "</br>";

//understand with "access modifiers" such as public, private & protected
//this thing also called 'encapsulation' when accessing date is restricted.
$habib = new Officer('Md. Habibul Islam');

echo "Habib full name is: ". $habib->getName();
echo "</br>";
//echo "Habib pin number is: ". $habib->get_pin_number();//private function can't access



//inheritance examples

$nick = new Worker('jonayet hasan');
echo $nick->getName();
echo "</br>";


$mark = new OnlineCustomer('Markcockroft');
echo $mark->name;



?>




</body>
</html>