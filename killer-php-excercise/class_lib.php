<?php
class Person{
    public $name;

    public function setName($new_name){
        $this->name = $new_name;
    }
    public function getName(){
        return $this->name;
    }
}

//step-14: crete an object with a constructor

class Employee{
    public $name;

    //creating a constructor methods.
    public function __construct($employees_name)
    {
        $this->name = $employees_name;
    }
    //setter function
    public function setName($new_name){
        $this->name = $new_name;
    }
    //getter function
    public function getName(){
        return $this->name;
    }
}

//understand with "access modifiers" such as public, private & protected
//this thing also called 'encapsulation' when accessing date is restricted.
class Officer{
    public $name; //when variable declare with 'var' keyword, it is                 considered as public.
    public $height;
    protected $social_insurance;
    //public $social_insurance;
    private $pin_numner;
    //public $pin_numner;

    public function __construct($officers_name)
    {
        $this->name = $officers_name;
    }

    public function setName($new_name){
        $this->name = $new_name;

    }

    public function getName(){
        return $this->name;

    }

    private function get_pin_number(){
        return $this->pin_numner;
    }
}


//Inheritance Examples
//extends is the word keyword that enables inheritance.
//class Worker extends Person{
//    public function __construct($workers_name)
//    {
//      $this->setName($workers_name);
//    }
//}

class Worker extends Person {
    public function __construct($Workers_name){
        $this->setName($Workers_name);
    }
}

//overriding method

class Customer {
    public $name;

    public function __construct($customers_name){
        $this->name = $customers_name;
    }

    protected function setName($new_name){
        if($this->name != "Jimmy Two Guns"){
            $this->name = strtoupper($new_name);
        }
    }

    public function getName(){
        return $this->name;
    }

}
//enables inheritance form Customer

class OnlineCustomer extends Customer {
    public function __constructor($onlineCustomers_name){
        $this->setName = ($onlineCustomers_name);
    }

    protected function setName($new_name){
        if($new_name == "Stefan Lamp"){
            $this->name = $new_name;
        }
    }
}

?>