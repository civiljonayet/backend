<?php

namespace App\Bank\Atm;

class Atm{
    public $location;
    public $managedby;

    public function idenfiers(){

    }
    public function transactions(){

    }

    public function setLocation($AtmLocation){
        $this->location = $AtmLocation;
    }
    public function getLocation(){
        return $this->location;
    }

    public function setManagedby($AtmManagedby){
        $this->managedby = $AtmManagedby;
    }
    public function getManagedby(){
        return $this->managedby;
    }
}
