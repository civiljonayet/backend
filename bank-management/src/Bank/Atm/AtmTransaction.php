<?php

namespace App\Bank\Atm;

class AtmTransaction{
    public $transactionId;
    public $date;
    public $type;
    public $amount;
    public $postBalance;

    public function modifies(){

    }

    public function setAtmTransactionId($atmTransactionId){
    $this->transactionId = $atmTransactionId;
}
    public function getAtmTransactionId(){
        return $this->transactionId;
    }

    public function setAtmTransactionDate($atmTransactionDate){
    $this->date = $atmTransactionDate;
}
    public function getAtmTransactionDate(){
        return $this->transactionId;
    }

    public function setAtmTransactionType($atmTransactionType){
        $this->type = $atmTransactionType;
    }
    public function getAtmTransactionType(){
        return $this->type;
    }

    public function setAtmTransactionAmount($atmTransactionAmount){
        $this->amount = $atmTransactionAmount;
    }
    public function getAtmTransactionAmount(){
        return $this->amount;
    }

    public function setAtmTransactionPostBalance($atmTransactionPostBalance){
        $this->postBalance = $atmTransactionPostBalance;
    }
    public function getAtmTransactionPostBalance(){
        return $this->postBalance;
    }
}
