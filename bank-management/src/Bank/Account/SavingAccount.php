<?php

namespace App\Bank\Account;

class SavingAccount{
    public $accountNumber;
    public $balance;

    public function setAccountNumber($savingAccountNumber){
        $this->accountNumber = $savingAccountNumber;
    }
    public function getAccountNumber(){
        return $this->accountNumber;
    }

    public function setBalance($savingAccountBalance){
        $this->balance = $savingAccountBalance;
    }
    public function getBalance(){
        return $this->balance;
    }
}
?>