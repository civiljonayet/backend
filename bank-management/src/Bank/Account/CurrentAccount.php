<?php

namespace App\Bank\Account;

class CurrentAccount{
    public $accountNumber;
    public $balance;

    public function withdraw(){

    }

    public function setAccountNumber($currentAccountNumber){
        $this->accountNumber = $currentAccountNumber;
    }

    public function getAccountNumber(){
        return $this->accountNumber;
    }

    public function setBalance($currentAccountBalance){
        $this->balance = $currentAccountBalance;
    }
    public function getBalance(){
        return $this->balance;
    }
}
