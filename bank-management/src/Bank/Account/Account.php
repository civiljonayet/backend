<?php

namespace App\Bank\Account;

class Account{
    public $number;
    public $balance;

    public function deposit(){

    }

    public function withdraw(){

    }

    public function createTransaction(){

    }

    public function setNumber($accountNumber){

    }
    public function getNumber(){
        return $this->number;
    }

    public function setBalance($accountBalance){
        $this->balance = $accountBalance;
    }
    public function getBalance(){
        return $this->balance;
    }
}
