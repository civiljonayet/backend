<?php

namespace App\Bank;

class Bank{
    public $code;
    public $address;



    public function setCode($bankCode){
        $this->code = $bankCode;
    }
    public function getCode(){
        return $this->code;
    }

    public function setAddress($bankAddress){
        $this->address = $bankAddress;
    }
    public function getAddress(){
        return $this->address;
    }

    public function manages(){

    }
    public function maintains(){

    }
}
