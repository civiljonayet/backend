<?php

namespace App\Customer;

class Customer{
    public $title;
    public $firstName;
    public $lastName;
    public $address;
    public $dob;
    public $cardNumber;
    public $pin;

    public function __construct($title, $firstName, $lastName) //Magic method
    {
        $this->title = $title;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function __toString() //magic method
    {
        // TODO: Implement __toString() method.
        return $this->title." ".$this->firstName." ".$this->lastName;
    }

    public function getFullName(){
        return $this->title." ".$this->firstName." ".$this->lastName;
    }

    public function setName($customerName){
        $this->name = $customerName;
    }
    public function getName(){
        return $this->name;
    }
    public function setAddress($customerAddress){
        $this->address = $customerAddress;
    }
    public function getAddress(){
        return $this->address;
    }
    public function setDob($customerDob){
        $this->dob = $customerDob;
    }
    public function getDob(){
        return $this->dob;
    }
    public function setCardNumber($customerCardNumber){
        $this->cardNumber = $customerCardNumber;
    }
    public function getCardNumber(){
        return $this->cardNumber;
    }
    public function setPin($customerPin){
        $this->pin = $customerPin;
    }
    public function getPin(){
        return $this->pin;
    }

    public function verifyPassword(){

    }
}
